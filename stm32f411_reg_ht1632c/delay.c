#include "delay.h"
#include "rcc_config.h"

void tim4_config (void)
{
	// 1. Enable Timer clock
	RCC->APB1ENR |= (1<<2);  // Enable the timer4 clock
	
	// 2. Set the prescalar and the ARR
	TIM4->PSC = 50-1;  // 50MHz/50 = 1 MHz ~~ 1 uS delay
	TIM4->ARR = 0xffff-1;  // MAX ARR value
	
	// 3. Enable the Timer, and wait for the update Flag to set
	TIM4->CR1 |= (1<<0); // Enable the Counter
	while (!(TIM4->SR & (1<<0)));  // UIF: Update interrupt flag..  This bit is set by hardware when the registers are updated
}

void delay_us (uint16_t us)
{
	TIM4->CNT = 0;
	while (TIM4->CNT < us);
}

void delay_ms (uint16_t ms)
{
	for (uint16_t i=0; i<ms; i++)
	{
		delay_us (1000); // delay of 1 ms
	}
}

