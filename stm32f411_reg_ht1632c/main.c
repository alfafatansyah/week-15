#include "main.h"
#include "rcc_config.h"
#include "delay.h"
#include "gpio_config.h"
#include "ht1632c.h"

uint8_t a, b, sym_count = 16;
uint16_t digit_seg[8], segment[14];;
char text[] = "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG        ";

uint16_t convert_char (char word);
void set_symbol (uint8_t number);
void print_segment(uint16_t header, uint32_t number);

int main (void)
{
	sys_clock_config();
	tim4_config();
	gpio_config();
	
	delay_ms(100);
	ht1632_init();
	
	while(1)
	{
		print_segment(0,0);
		
		a = sizeof(text);
		int temp = text[0];
		for(int i=0; i<a-2; i++)
		{
			text[i] = text[i+1];
		}
		text[a-2] = temp;			
		
		b = text[0];
		
		sym_count--;
		if(sym_count > 16) sym_count = 16;
		
		delay_ms(500);
	}
}

uint16_t convert_char (char word)
{
	switch(word)
	{
		case (0x30):
			return n_0;
			break;
		case (0x31):
			return n_1;
			break;
		case (0x32):
			return n_2;
			break;
		case (0x33):
			return n_3;
			break;
		case (0x34):
			return n_4;
			break;
		case (0x35):
			return n_5;
			break;
		case (0x36):
			return n_6;
			break;
		case (0x37):
			return n_7;
			break;
		case (0x38):
			return n_8;
			break;
		case (0x39):
			return n_9;
			break;
		case (0x41):
			return n_A;
			break;
		case (0x42):
			return n_B;
			break;
		case (0x43):
			return n_C;
			break;
		case (0x44):
			return n_D;
			break;
		case (0x45):
			return n_E;
			break;
		case (0x46):
			return n_F;
			break;
		case (0x47):
			return n_G;
			break;
		case (0x48):
			return n_H;
			break;
		case (0x49):
			return n_I;
			break;
		case (0x4A):
			return n_J;
			break;
		case (0x4B):
			return n_K;
			break;
		case (0x4C):
			return n_L;
			break;
		case (0x4D):
			return n_M;
			break;
		case (0x4E):
			return n_N;
			break;
		case (0x4F):
			return n_O;
			break;
		case (0x50):
			return n_P;
			break;
		case (0x51):
			return n_Q;
			break;
		case (0x52):
			return n_R;
			break;
		case (0x53):
			return n_S;
			break;
		case (0x54):
			return n_T;
			break;
		case (0x55):
			return n_U;
			break;
		case (0x56):
			return n_V;
			break;
		case (0x57):
			return n_W;
			break;
		case (0x58):
			return n_X;
			break;
		case (0x59):
			return n_Y;
			break;
		case (0x5A):
			return n_Z;
			break;
		default:
			return 0x0000;
			break;
	}
}

void set_symbol (uint8_t number)
{
	switch(number)
	{
		case(0):
			set_play;
			break;
		case(1):
			set_repeat;
			break;
		case(2):
			set_mic;
			break;
		case(3):
			set_bt;
			break;
		case(4):
			set_fm;
			break;
		case(5):
			set_card;
			break;
		case(6):
			set_usb;
			break;
		case(7):
			set_wifi;
			break;
		case(8):
			set_music;
			break;
		case(9):
			set_video;
			break;
		case(10):
			set_picture;
			break;
		case(11):
			set_pause;
			break;
		case(12):
			set_rand;
			break;
		case(13):
			set_L;
			break;
		case(14):
			set_R;
			break;
		case(15):
			set_bazz;
			break;
		case(16):
			set_mhz;
			break;		
		default:
			break;
	}
}

void print_segment(uint16_t header, uint32_t number)
{	
	digit_seg[0] = convert_char(text[0]);
	digit_seg[1] = convert_char(text[1]);
	digit_seg[2] = convert_char(text[2]);
	digit_seg[3] = convert_char(text[3]);
	digit_seg[4] = convert_char(text[4]);
	digit_seg[5] = convert_char(text[5]);
	digit_seg[6] = convert_char(text[6]);
	digit_seg[7] = convert_char(text[7]);	
	for(int i = 0; i < 14; i++)
		segment[i] = 0;	
	int p = 1;
	for(int i = 0; i < 14; i++)
	{
		for(int j = 0; j < 8; j++)
		{
			segment[i] <<= 1;
			segment[i] = (digit_seg[j] & p) ? segment[i] | 1 : segment[i] | 0;
		}
		p = p << 1;
		segment[i] <<= 8;
	}		
	set_symbol(sym_count);	
	p = 0;
	for(int i = 0x00; i <= 0x34; i+=4)
	{
		write_data(id_write, i, segment[p]);
		p++;
	}
}