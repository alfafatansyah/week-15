#include "ht1632c.h"

void set_logic (uint8_t logic)
{
	if(logic == 1) set_pin_DATA;
	else reset_pin_DATA;
}

void write_code (uint8_t id_data, uint8_t cmd_data)
{
	uint16_t temp = (id_data<<9) | (cmd_data<<1);
	set_pin_CS;
	for(int i = 1<<11; i>0; i >>= 1)
	{
		reset_pin_CLK;
		set_logic((temp & i) ? 1 : 0);
		set_pin_CLK;
	}
	reset_pin_CS;
}

void write_data (uint8_t id_data, uint8_t add_data, uint16_t data_data)
{
	uint32_t temp = (id_data<<23) | (add_data<<16) | (data_data);
	set_pin_CS;	
	for(int i = 1<<25; i>0; i >>= 1)
	{
		reset_pin_CLK;
		set_logic((temp & i) ? 1 : 0);
		set_pin_CLK;
	}
	reset_pin_CS;
}

void ht1632_init (void)
{
	write_code(id_cmd, code_sys_dis);
	write_code(id_cmd, code_com_nmos_16);
	write_code(id_cmd, code_master_mode);
	write_code(id_cmd, code_sys_en);
	write_code(id_cmd, code_led_on);
	for(int i = 0; i <= 0x5C; i+=4)
	{
		write_data(id_write, i, 0xFFFF);
	}
}