#include "gpio_config.h"

void gpio_config (void)
{
	// 1. Enable the GPIOA CLOCK
	RCC->AHB1ENR |= (1<<0);
	
	// 2. Set the Pin as OUTPUT
	GPIOA->MODER |= (1<<(4*2)); // pin PB4(bits 9:4) as Output (01)
	GPIOA->MODER |= (1<<(5*2)); // pin PB5(bits 11:10) as Output (01)
	GPIOA->MODER |= (1<<(6*2)); // pin PB6(bits 13:12) as Output (01)
	GPIOA->MODER |= (1<<(7*2)); // pin PB7(bits 15:14) as Output (01)
	
	// 3. Configure the OUTPUT MODE
	GPIOA->OTYPER = 0;
	GPIOA->OSPEEDR = 0;	
	
	// 1. Enable the GPIOC CLOCK
	RCC->AHB1ENR |= (1<<2);
	
	// 2. Set the Pin as OUTPUT
	GPIOC->MODER |= (1<<(13*2)); // pin PB13(bits 27:26) as Output (01)
	
	// 3. Configure the OUTPUT MODE
	GPIOC->OTYPER = 0;
	GPIOC->OSPEEDR = 0;	
}