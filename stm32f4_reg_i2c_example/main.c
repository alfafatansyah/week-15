#include "rcc_config.h"
#include "delay.h"
#include "i2c.h"
#include "i2c_lcd.h"

void gpio_init (void)
{
	RCC->AHB1ENR = (1<<2);
	GPIOC->MODER = (1<<(13*2));
}

int main (void)
{
	sys_clock_config();
	tim2_config();
	i2c_config();
	gpio_init();
	
	lcd_init ();
	lcd_put_cur (0,0);
	lcd_send_string ("hello");	
	
	while(1)
	{
		GPIOC->BSRR |= (1<<13);
		delay_ms (1000);
		GPIOC->BSRR |= ((1<<13) <<16);
		delay_ms (1000);
	}
}