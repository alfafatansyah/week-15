
/**
  ******************************************************************************

  Timer Setup for Delay in STM32F446RE
  Author:   ControllersTech
  Updated:  2nd Aug 2020

  ******************************************************************************
  Copyright (C) 2017 ControllersTech.com

  This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  of the GNU General Public License version 3 as published by the Free Software Foundation.
  This software library is shared with public for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  or indirectly by this software, read more about this on the GNU General Public License.

  ******************************************************************************
*/

#include "delay.h"
#include "rcc_config.h"

void tim2_config (void)
{
	/************** STEPS TO FOLLOW *****************
	1. Enable Timer clock
	2. Set the prescalar and the ARR
	3. Enable the Timer, and wait for the update Flag to set
	************************************************/

// 1. Enable Timer clock
	RCC->APB1ENR |= (1<<0);  // Enable the timer2 clock
	
// 2. Set the prescalar and the ARR
	TIM2->PSC = 50-1;  // 50MHz/50 = 1 MHz ~~ 1 uS delay
	TIM2->ARR = 0xffff-1;  // MAX ARR value
	
// 3. Enable the Timer, and wait for the update Flag to set
	TIM2->CR1 |= (1<<0); // Enable the Counter
	while (!(TIM2->SR & (1<<0)));  // UIF: Update interrupt flag..  This bit is set by hardware when the registers are updated
}

void delay_us (uint16_t us)
{
	/************** STEPS TO FOLLOW *****************
	1. RESET the Counter
	2. Wait for the Counter to reach the entered value. As each count will take 1 us, 
		 the total waiting time will be the required us delay
	************************************************/
	TIM2->CNT = 0;
	while (TIM2->CNT < us);
}

void delay_ms (uint16_t ms)
{
	for (uint16_t i=0; i<ms; i++)
	{
		delay_us (1000); // delay of 1 ms
	}
}

