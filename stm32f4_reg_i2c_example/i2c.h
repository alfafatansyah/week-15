

/**
  ******************************************************************************

  I2C Setup For STM32F446RE
  Author:   ControllersTech
  Updated:  31st Jan 2020

  ******************************************************************************
  Copyright (C) 2017 ControllersTech.com

  This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  of the GNU General Public License version 3 as published by the Free Software Foundation.
  This software library is shared with public for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  or indirectly by this software, read more about this on the GNU General Public License.

  ******************************************************************************
*/

#include <stdint.h>

void i2c_config (void);

void i2c_start (void);

void i2c_write (uint8_t data);

void i2c_address (uint8_t Address);

void i2c_stop (void);

void i2c_write_multi (uint8_t *data, uint8_t size);

void i2c_read (uint8_t Address, uint8_t *buffer, uint8_t size);
